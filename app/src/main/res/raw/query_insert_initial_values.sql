insert into car_manufacturer (manufacturer_id, manufacturer_name) values (1, 'Japan');
insert into car_manufacturer (manufacturer_id, manufacturer_name) values (2, 'America');
insert into car_manufacturer (manufacturer_id, manufacturer_name) values (3, 'Germany');

INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (1, 1,  'Toyota');
INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (2, 1,  'Mazda');
INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (3, 1,  'Acura');

INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (4, 2,  'Tesla');
INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (5, 2,  'Ford');

INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (6, 3,  'BMW');
INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (7, 3,  'Audi');
INSERT INTO manufacturer_brand (brand_id, manufacturer_id, brand_name) VALUES (8, 3,  'Volkswagen');



INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (1, 14625, 'Toyote Camty SE', 'blue', 0, '2.5L', '6-Speed Automatic', 1);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (2, 25000, 'Mazda CX-9 Touring', 'gray', 13712, '2.5L', '6-Speed Automatic', 2);


INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (3, 56777, 'Acura NSX Sport', 'black', 34027, '3L, V6', '5-Speed Manual', 3);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (4, 67500, 'Tesla Model S 750', 'black', 19220, 'Electric', '1-Speed Automatic', 4);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (5, 84052, 'Tesla Model X P90D', 'gray', 47945, 'Electric', '1-Speed Automatic', 4);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (6, 129977, 'Tesla Model X P100D', 'dark blue', 9746, 'Electric', '1-Speed Automatic', 4);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (7, 41995, 'Tesla Model S Base', 'white', 43995, 'Electric', '1-Speed Automatic', 4);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (8, 24995, 'Ford Mustang GT Premium', 'black', 43595, '5.0L V8 32V', 'G-Speed Manual', 5);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (9, 31539, 'Ford F-150 XLT', 'blue', 31539, '2.7L V6 24V', '10-Speed Automatic', 5);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (10, 29980, 'BMW 335 i', 'alpine white', 24234, '3.0L', '8-Speed Automatic', 6);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (11, 64480, 'Audi RS7 Prestige', 'gray', 39711, 'V-8', '8-Speed Automatic', 7);

INSERT INTO car (car_id, price, name, color, mileage, engine, transmission, brand_id)
	VALUES (12, 18995, 'Volkswagen Golf GTT 2.0T', 'black', 26830, '2.0L', '6-Speed Manual', 8);