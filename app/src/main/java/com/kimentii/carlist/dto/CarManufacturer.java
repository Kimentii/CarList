package com.kimentii.carlist.dto;

import com.google.gson.annotations.SerializedName;
import com.kimentii.carlist.database.CarDbSchema.CarManufacturerTable;

public class CarManufacturer {
    @SerializedName(CarManufacturerTable.Cols.MANUFACTURER_ID)
    private int id;
    @SerializedName(CarManufacturerTable.Cols.NAME)
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarManufacturer that = (CarManufacturer) o;
        return id == that.id;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
