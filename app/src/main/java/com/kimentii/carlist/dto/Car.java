package com.kimentii.carlist.dto;

import com.google.gson.annotations.SerializedName;
import com.kimentii.carlist.database.CarDbSchema.CarManufacturerTable;
import com.kimentii.carlist.database.CarDbSchema.CarTable;
import com.kimentii.carlist.database.CarDbSchema.ManufacturerBrandTable;

import java.util.ArrayList;

public class Car {
    @SerializedName(CarTable.Cols.CAR_ID)
    private int carId;
    @SerializedName(CarTable.Cols.PRICE)
    private int price;
    @SerializedName(CarTable.Cols.NAME)
    private String name;
    @SerializedName(CarTable.Cols.COLOR)
    private String color;
    @SerializedName(CarTable.Cols.MILEAGE)
    private int mileage;
    @SerializedName(CarTable.Cols.ENGINE)
    private String engine;
    @SerializedName(CarTable.Cols.TRANSMISSION)
    private String transmission;

    @SerializedName(CarTable.Cols.BRAND_ID)
    private int brandId;
    @SerializedName(ManufacturerBrandTable.Cols.NAME)
    private String brandName;
    @SerializedName(ManufacturerBrandTable.Cols.MANUFACTURER_ID)
    private int manufacturerId;
    @SerializedName(CarManufacturerTable.Cols.NAME)
    private String manufacturerName;

    private transient ArrayList<Photo> mPhotoArrayList;

    public ArrayList<Photo> getPhotoArrayList() {
        return mPhotoArrayList;
    }

    public void setPhotoArrayList(ArrayList<Photo> photoArrayList) {
        mPhotoArrayList = photoArrayList;
    }

    public Brand getBrand() {
        Brand brand = new Brand();
        brand.setId(brandId);
        brand.setManufacturerId(manufacturerId);
        brand.setName(brandName);
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brandId = brand.getId();
        this.manufacturerId = brand.getManufacturerId();
        this.brandName = brand.getName();
    }

    public CarManufacturer getManufacturer() {
        CarManufacturer manufacturer = new CarManufacturer();
        manufacturer.setId(manufacturerId);
        manufacturer.setName(manufacturerName);
        return manufacturer;
    }

    public void setManufacturer(CarManufacturer manufacturer) {
        this.manufacturerId = manufacturer.getId();
        this.manufacturerName = manufacturer.getName();
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }
}
