package com.kimentii.carlist.dto;

import com.google.gson.annotations.SerializedName;
import com.kimentii.carlist.database.CarDbSchema.CarTable;
import com.kimentii.carlist.database.CarDbSchema.PhotoTable;

public class Photo {
    @SerializedName(CarTable.Cols.CAR_ID)
    private int carId;
    @SerializedName(PhotoTable.Cols.PHOTO_URL)
    private String photoUrl;

    public Photo(int carId, String photoUrl) {
        this.carId = carId;
        this.photoUrl = photoUrl;
    }

    public int getCarId() {
        return carId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
