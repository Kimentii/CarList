package com.kimentii.carlist.dto;

import com.google.gson.annotations.SerializedName;
import com.kimentii.carlist.database.CarDbSchema.ManufacturerBrandTable;

public class Brand {
    @SerializedName(ManufacturerBrandTable.Cols.BRAND_ID)
    private int id;
    @SerializedName(ManufacturerBrandTable.Cols.MANUFACTURER_ID)
    private int manufacturerId;
    @SerializedName(ManufacturerBrandTable.Cols.NAME)
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brand brand = (Brand) o;
        return id == brand.id &&
                manufacturerId == brand.manufacturerId;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
