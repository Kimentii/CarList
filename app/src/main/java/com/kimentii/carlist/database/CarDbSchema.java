package com.kimentii.carlist.database;

public class CarDbSchema {
    public static final class CarTable {
        public static final String NAME = "car";

        public static final class Cols {
            public static final String CAR_ID = "car_id";
            public static final String PRICE = "price";
            public static final String NAME = "name";
            public static final String COLOR = "color";
            public static final String MILEAGE = "mileage";
            public static final String ENGINE = "engine";
            public static final String TRANSMISSION = "transmission";
            public static final String BRAND_ID = "brand_id";
        }
    }

    public static final class CarManufacturerTable {
        public static final String NAME = "car_manufacturer";

        public static final class Cols {
            public static final String MANUFACTURER_ID = "manufacturer_id";
            public static final String NAME = "manufacturer_name";
        }
    }

    public static final class ManufacturerBrandTable {
        public static final String NAME = "manufacturer_brand";

        public static final class Cols {
            public static final String BRAND_ID = "brand_id";
            public static final String MANUFACTURER_ID = "manufacturer_id";
            public static final String NAME = "brand_name";
        }
    }

    public static final class PhotoTable {
        public static final String NAME = "photo";

        public static final class Cols {
            public static final String PHOTO_URL = "photo_url";
            public static final String CAR_ID = "car_id";
        }
    }
}