package com.kimentii.carlist.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.Pair;

import com.kimentii.carlist.BuildConfig;
import com.kimentii.carlist.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.kimentii.carlist.database.CarDbSchema.CarManufacturerTable;
import static com.kimentii.carlist.database.CarDbSchema.CarTable;
import static com.kimentii.carlist.database.CarDbSchema.ManufacturerBrandTable;
import static com.kimentii.carlist.database.CarDbSchema.PhotoTable;

public class CarBaseHelper extends SQLiteOpenHelper {
    private static final String TAG = CarBaseHelper.class.getSimpleName();

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "carBase.db";

    private Context mContext;

    public CarBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        mContext = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createCarTable(sqLiteDatabase);
        createCarManufacturerTable(sqLiteDatabase);
        createManufacturerBrandTable(sqLiteDatabase);
        createPhotoTable(sqLiteDatabase);

        try {
            ArrayList<String> insertQuery = getQueriesFromFile(mContext, R.raw.query_insert_initial_values);
            for (String i : insertQuery) {
                sqLiteDatabase.execSQL(i);
            }
            ArrayList<String> insertInPhotoTable = getQueriesFromFile(mContext, R.raw.photo_table_initial_values);
            for (String i : insertInPhotoTable) {
                sqLiteDatabase.execSQL(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private void createCarTable(SQLiteDatabase sqLiteDatabase) {
        ArrayList<Pair<String, String>> carTableColumns = new ArrayList<>();
        carTableColumns.add(new Pair<>(CarTable.Cols.CAR_ID, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        carTableColumns.add(new Pair<>(CarTable.Cols.PRICE, "INTEGER"));
        carTableColumns.add(new Pair<>(CarTable.Cols.NAME, "TEXT"));
        carTableColumns.add(new Pair<>(CarTable.Cols.COLOR, "TEXT"));
        carTableColumns.add(new Pair<>(CarTable.Cols.MILEAGE, "INTEGER"));
        carTableColumns.add(new Pair<>(CarTable.Cols.ENGINE, "TEXT"));
        carTableColumns.add(new Pair<>(CarTable.Cols.TRANSMISSION, "TEXT"));
        carTableColumns.add(new Pair<>(CarTable.Cols.BRAND_ID, "INTEGER"));
        String constraint2 = String.format("FOREIGN KEY (%s)\n" +
                "REFERENCES %s (%s)", CarTable.Cols.BRAND_ID, ManufacturerBrandTable.NAME, ManufacturerBrandTable.Cols.BRAND_ID);

        String query = getCreateTableQuery(CarTable.NAME,
                carTableColumns,
                constraint2);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createCarTable: query: " + query);
        }
        sqLiteDatabase.execSQL(query);
    }

    private void createCarManufacturerTable(SQLiteDatabase sqLiteDatabase) {
        ArrayList<Pair<String, String>> carTableColumns = new ArrayList<>();
        carTableColumns.add(new Pair<>(CarManufacturerTable.Cols.MANUFACTURER_ID, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        carTableColumns.add(new Pair<>(CarManufacturerTable.Cols.NAME, "TEXT"));

        String query = getCreateTableQuery(CarManufacturerTable.NAME, carTableColumns);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createCarManufacturerTable: query: " + query);
        }
        sqLiteDatabase.execSQL(query);
    }

    private void createManufacturerBrandTable(SQLiteDatabase sqLiteDatabase) {
        ArrayList<Pair<String, String>> tableColumns = new ArrayList<>();
        tableColumns.add(new Pair<>(ManufacturerBrandTable.Cols.BRAND_ID, "INTEGER PRIMARY KEY AUTOINCREMENT"));
        tableColumns.add(new Pair<>(ManufacturerBrandTable.Cols.MANUFACTURER_ID, "INTEGER"));
        tableColumns.add(new Pair<>(ManufacturerBrandTable.Cols.NAME, "TEXT"));
        String constraint1 = String.format("FOREIGN KEY (%s)\n" +
                        "REFERENCES %s (%s)", ManufacturerBrandTable.Cols.MANUFACTURER_ID, CarManufacturerTable.NAME,
                CarManufacturerTable.Cols.MANUFACTURER_ID);

        String query = getCreateTableQuery(ManufacturerBrandTable.NAME, tableColumns,
                constraint1);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createManufacturerBrandTable: query: " + query);
        }
        sqLiteDatabase.execSQL(query);
    }

    private void createPhotoTable(SQLiteDatabase sqLiteDatabase) {
        ArrayList<Pair<String, String>> tableColumns = new ArrayList<>();
        tableColumns.add(new Pair<>(PhotoTable.Cols.CAR_ID, "INTEGER"));
        tableColumns.add(new Pair<>(PhotoTable.Cols.PHOTO_URL, "TEXT"));
        String constraint1 = "CONSTRAINT pk_photo_id PRIMARY KEY (car_id, photo_url)";
        String constraint2 = "FOREIGN KEY (car_id)\n" +
                "REFERENCES car (car_id)";

        String query = getCreateTableQuery(PhotoTable.NAME, tableColumns,
                constraint1, constraint2);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createPhotoTable: query: " + query);
        }
        sqLiteDatabase.execSQL(query);
    }

    private String getCreateTableQuery(String tableName, ArrayList<Pair<String, String>> columns,
                                       String... additions) {
        StringBuilder stringBuilder = new StringBuilder("CREATE TABLE " + tableName + "(");
        int i = 0;
        for (; i < columns.size() - 1; i++) {
            stringBuilder.append(columns.get(i).first).append(" ").append(columns.get(i).second).append(",\n");
        }
        stringBuilder.append(columns.get(i).first).append(" ").append(columns.get(i).second);
        if (additions.length > 0) {
            stringBuilder.append(",\n");
            int j = 0;
            for (; j < additions.length - 1; j++) {
                stringBuilder.append(additions[j]).append(",");
            }
            stringBuilder.append(additions[j]);
        }
        stringBuilder.append(");\n");
        return stringBuilder.toString();
    }

    private ArrayList<String> getQueriesFromFile(Context context, int resourceId) throws IOException {
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));

        StringBuilder query = new StringBuilder();
        while (insertReader.ready()) {
            String insertStmt = insertReader.readLine();
            query.append(insertStmt).append("\n");
        }
        insertReader.close();

        ArrayList<String> result = new ArrayList<>();
        String[] queries = query.toString().split(";");
        for (String q : queries) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "getQueriesFromFile: " + q);
            }
            if (q.trim().length() > 0) {
                result.add(q + ";");
            }
        }

        return result;
    }
}
