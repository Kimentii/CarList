package com.kimentii.carlist.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kimentii.carlist.R;
import com.kimentii.carlist.dto.Car;
import com.kimentii.carlist.fragments.CarDetailsFragment;
import com.kimentii.carlist.fragments.CarListFragment;

public class CarDetailsActivity extends SingleFragmentActivity {
    private static final String EXTRA_CAR_ID = "com.kimentii.carlist.extras.EXTRA_CAR_ID";

    @Override
    protected Fragment createFragment() {
        return CarDetailsFragment.newInstance(getIntent().getExtras().getInt(EXTRA_CAR_ID));
    }

    public static Intent getStartIntent(Context context, int carId) {
        Intent intent = new Intent(context, CarDetailsActivity.class);
        intent.putExtra(EXTRA_CAR_ID, carId);
        return intent;
    }
}
