package com.kimentii.carlist.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.kimentii.carlist.fragments.CreateCarFragment;

public class CreateCarActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return CreateCarFragment.newInstance();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, CreateCarActivity.class);
    }
}
