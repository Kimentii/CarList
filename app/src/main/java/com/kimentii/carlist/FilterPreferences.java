package com.kimentii.carlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.CarManufacturer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FilterPreferences {

    private static final String PREF_SORT_BY_COLUMN = "PREF_SORT_BY_COLUMN";
    private static final String PREF_FILTER_TYPE = "PREF_FILTER_TYPE";
    private static final String PREF_FILTER_VALUES = "PREF_FILTER_VALUES";

    public static final int FILTER_TYPE_NONE = -1;
    public static final int FILTER_TYPE_MANUFACTURER = 0;
    public static final int FILTER_TYPE_BRAND = 1;

    public static String getSortByColumn(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_SORT_BY_COLUMN, null);
    }

    public static void setSortByColumn(Context context, String columnName) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(PREF_SORT_BY_COLUMN, columnName).apply();
    }

    public static int getFilterType(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(PREF_FILTER_TYPE, FILTER_TYPE_NONE);
    }

    public static ArrayList<CarManufacturer> getManufacturerFilterValues(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = sharedPreferences.getStringSet(PREF_FILTER_VALUES, null);
        ArrayList<CarManufacturer> manufacturers = null;
        if (set != null) {
            manufacturers = new ArrayList<>();
            Gson gson = new Gson();
            for (String s : set) {
                manufacturers.add(gson.fromJson(s, CarManufacturer.class));
            }
        }
        return manufacturers;
    }

    public static ArrayList<Brand> getBrandFilterValues(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = sharedPreferences.getStringSet(PREF_FILTER_VALUES, null);
        ArrayList<Brand> brands = null;
        if (set != null) {
            brands = new ArrayList<>();
            Gson gson = new Gson();
            for (String s : set) {
                brands.add(gson.fromJson(s, Brand.class));
            }
        }
        return brands;
    }

    public static void setManufacturerFilterValues(Context context, ArrayList<CarManufacturer> carManufacturers) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> values = new HashSet<>();
        if (carManufacturers != null) {
            Gson gson = new Gson();
            for (CarManufacturer m : carManufacturers) {
                values.add(gson.toJson(m));
            }
            setFilterType(context, FILTER_TYPE_MANUFACTURER);
        } else {
            setFilterType(context, FILTER_TYPE_NONE);
        }
        sharedPreferences.edit().putStringSet(PREF_FILTER_VALUES, values).apply();
    }

    public static void setBrandFilterValues(Context context, ArrayList<Brand> brands) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> values = new HashSet<>();
        if (brands != null) {
            Gson gson = new Gson();
            for (Brand b : brands) {
                values.add(gson.toJson(b));
            }
            setFilterType(context, FILTER_TYPE_BRAND);
        } else {
            setFilterType(context, FILTER_TYPE_NONE);
        }
        sharedPreferences.edit().putStringSet(PREF_FILTER_VALUES, values).apply();
    }

    private static void setFilterType(Context context, int filterType) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putInt(PREF_FILTER_TYPE, filterType).apply();
    }
}
