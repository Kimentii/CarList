package com.kimentii.carlist;

import com.kimentii.carlist.dto.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class CarImageSliderAdapter extends SliderAdapter {
    private ArrayList<Photo> mPhotos;

    public CarImageSliderAdapter(ArrayList<Photo> carPhotos) {
        this.mPhotos = carPhotos;
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {
        Picasso.get().load(mPhotos.get(position).getPhotoUrl())
                .error(R.drawable.no_image).into(imageSlideViewHolder.imageView);
    }
}