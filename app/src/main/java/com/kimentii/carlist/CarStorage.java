package com.kimentii.carlist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.kimentii.carlist.database.CarBaseHelper;
import com.kimentii.carlist.database.CarDbSchema.CarManufacturerTable;
import com.kimentii.carlist.database.CarDbSchema.CarTable;
import com.kimentii.carlist.database.CarDbSchema.ManufacturerBrandTable;
import com.kimentii.carlist.database.CarDbSchema.PhotoTable;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.Car;
import com.kimentii.carlist.dto.CarManufacturer;
import com.kimentii.carlist.dto.Photo;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class CarStorage {
    private static final String TAG = CarStorage.class.getSimpleName();

    private static CarStorage sCarStorage;
    private SQLiteDatabase mSQLiteDatabase;

    private CarStorage(Context context) {
        mSQLiteDatabase = new CarBaseHelper(context.getApplicationContext()).getWritableDatabase();
    }

    public static synchronized CarStorage getInstance(Context context) {
        if (sCarStorage == null) {
            sCarStorage = new CarStorage(context);
        }
        return sCarStorage;
    }

    public ArrayList<Car> getCarsFilteredByBrand(@Nullable String sortBy,
                                                 @Nullable ArrayList<Brand> brands) {
        StringBuilder whereClause = new StringBuilder();
        if (brands != null && !brands.isEmpty()) {
            whereClause.append(ManufacturerBrandTable.Cols.NAME + " IN (");
            int i = 0;
            for (; i < brands.size() - 1; i++) {
                whereClause.append("\'").append(brands.get(i).getName()).append("\', ");
            }
            whereClause.append("\'").append(brands.get(i).getName()).append("\') ");
        }
        return getCars(whereClause.toString(), null, sortBy);
    }

    public ArrayList<Car> getCarsFilteredByManufacturer(@Nullable String sortBy,
                                                        @Nullable ArrayList<CarManufacturer> manufacturers) {
        StringBuilder whereClause = new StringBuilder();
        if (manufacturers != null && !manufacturers.isEmpty()) {
            whereClause.append(CarManufacturerTable.Cols.NAME + " IN (");
            int i = 0;
            for (; i < manufacturers.size() - 1; i++) {
                whereClause.append("\'").append(manufacturers.get(i).getName()).append("\', ");
            }
            whereClause.append("\'").append(manufacturers.get(i).getName()).append("\') ");
        }
        return getCars(whereClause.toString(), null, sortBy);
    }

    public Car getCar(int carId) {
        ArrayList<Car> queryResult = getCars(CarTable.Cols.CAR_ID + " =? ",
                new String[]{String.valueOf(carId)}, null);
        Car car = null;
        if (queryResult != null && queryResult.size() > 0) {
            car = queryResult.get(0);
        }
        return car;
    }

    public void updateCar(Car car) {
        mSQLiteDatabase.update(CarTable.NAME,
                getContentValues(car),
                CarTable.Cols.CAR_ID + " =? ",
                new String[]{String.valueOf(car.getCarId())});
    }

    private ContentValues getContentValues(Car car) {
        ContentValues values = new ContentValues();
        values.put(CarTable.Cols.NAME, car.getName());
        values.put(CarTable.Cols.PRICE, car.getPrice());
        values.put(CarTable.Cols.BRAND_ID, car.getBrand().getId());
        values.put(CarTable.Cols.COLOR, car.getColor());
        values.put(CarTable.Cols.MILEAGE, car.getMileage());
        values.put(CarTable.Cols.ENGINE, car.getEngine());
        values.put(CarTable.Cols.TRANSMISSION, car.getTransmission());
        return values;
    }

    public File getPhotoFile(Context context) {
        File externalFilesDir = context
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            return null;
        }
        return new File(externalFilesDir, UUID.randomUUID().toString() + ".jpg");
    }

    public long addCar(Car car) {
        return mSQLiteDatabase.insert(CarTable.NAME,
                null,
                getContentValues(car));
    }

    public long addCarPhoto(long carId, String photoUrl) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PhotoTable.Cols.CAR_ID, carId);
        contentValues.put(PhotoTable.Cols.PHOTO_URL, photoUrl);
        return mSQLiteDatabase.insert(PhotoTable.NAME,
                null,
                contentValues);
    }

    private ArrayList<Car> getCars(String whereClause, String[] whereArgs, String orderBy) {
        // hack
        Cursor cursor = mSQLiteDatabase.query(
                CarTable.NAME +
                        " INNER JOIN manufacturer_brand ON car.brand_id = manufacturer_brand.brand_id "
                        + "INNER JOIN car_manufacturer ON manufacturer_brand.manufacturer_id = car_manufacturer.manufacturer_id ",
                null,
                whereClause,
                whereArgs,
                null,
                null,
                orderBy
        );
//        Cursor cursor = mSQLiteDatabase.rawQuery("SELECT * FROM car \n" +
//                "\tINNER JOIN manufacturer_brand ON car.brand_id = manufacturer_brand.brand_id\n" +
//                "    INNER JOIN car_manufacturer ON manufacturer_brand.manufacturer_id" +
//                " = car_manufacturer.manufacturer_id;", null);
        JSONArray jsonArray = cursorToJsonArray(cursor);
        ArrayList<Car> cars = getArrayFromJsonArray(jsonArray, Car.class);
        ArrayList<Photo> photos;
        for (Car car : cars) {
            photos = getCarPhotos(
                    PhotoTable.Cols.CAR_ID + " =? ",
                    new String[]{String.valueOf(car.getCarId())}
            );
            car.setPhotoArrayList(photos);
        }
        return cars;
    }

    public ArrayList<CarManufacturer> getCarManufacturers() {
        Cursor cursor = mSQLiteDatabase.query(CarManufacturerTable.NAME,
                null, null, null, null, null, null);
        JSONArray jsonArray = cursorToJsonArray(cursor);
        return getArrayFromJsonArray(jsonArray, CarManufacturer.class);
    }

    public ArrayList<Brand> getBrands() {
        Cursor cursor = mSQLiteDatabase.query(ManufacturerBrandTable.NAME,
                null, null, null, null, null, null);
        JSONArray jsonArray = cursorToJsonArray(cursor);
        return getArrayFromJsonArray(jsonArray, Brand.class);
    }

    public ArrayList<Brand> getManufacturerBrands(CarManufacturer carManufacturer) {
        Cursor cursor = mSQLiteDatabase.query(ManufacturerBrandTable.NAME,
                null,
                CarManufacturerTable.Cols.MANUFACTURER_ID + " =? ",
                new String[]{String.valueOf(carManufacturer.getId())}, null, null, null);
        JSONArray jsonArray = cursorToJsonArray(cursor);
        return getArrayFromJsonArray(jsonArray, Brand.class);
    }

    private ArrayList<Photo> getCarPhotos(String whereClause, String[] whereArgs) {
        Cursor cursor = mSQLiteDatabase.query(PhotoTable.NAME,
                null, whereClause, whereArgs, null, null, null);
        JSONArray jsonArray = cursorToJsonArray(cursor);
        return getArrayFromJsonArray(jsonArray, Photo.class);
    }

    private JSONArray cursorToJsonArray(Cursor cursor) {
        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        switch (cursor.getType(i)) {
                            case Cursor.FIELD_TYPE_BLOB:
                                rowObject.put(cursor.getColumnName(i), cursor.getBlob(i).toString());
                                break;
                            case Cursor.FIELD_TYPE_FLOAT:
                                rowObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                                break;
                            case Cursor.FIELD_TYPE_INTEGER:
                                rowObject.put(cursor.getColumnName(i), cursor.getLong(i));
                                break;
                            case Cursor.FIELD_TYPE_NULL:
                                rowObject.put(cursor.getColumnName(i), null);
                                break;
                            case Cursor.FIELD_TYPE_STRING:
                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                                break;
                        }
//                        rowObject.put(cursor.getColumnName(i),
//                                cursor.getString(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        return resultSet;
    }

    private <T> ArrayList<T> getArrayFromJsonArray(JSONArray jsonArray, Class<T> tClass) {
        ArrayList<T> arrayList = new ArrayList<>();
        Gson gson = new Gson();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                arrayList.add(gson.fromJson(jsonArray.getJSONObject(i).toString(), tClass));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getArrayFromJsonArray: JsonSyntaxException!!!!!: " + tClass.getName());
                }
                e.printStackTrace();
            }
        }
        return arrayList;
    }
}
