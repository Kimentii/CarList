package com.kimentii.carlist.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.kimentii.carlist.CarStorage;
import com.kimentii.carlist.FilterPreferences;
import com.kimentii.carlist.R;
import com.kimentii.carlist.database.CarDbSchema;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.CarManufacturer;

import java.util.ArrayList;

public class FilterDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    private OnFilterApplyListener mOnFilterApplyListener;
    private ListView mFilterItemsListView;

    private Spinner mSortSpinner;
    private Spinner mFilterSpinner;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_filter_dialog)
                .setPositiveButton(R.string.action_apply, this)
                .setNegativeButton(R.string.action_cancel, this);

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View rootView = layoutInflater.inflate(R.layout.dialog_fragment_filter, null);

        mSortSpinner = rootView.findViewById(R.id.spinner_sort_values);
        mFilterSpinner = rootView.findViewById(R.id.spinner_filter_values);
        mFilterItemsListView = rootView.findViewById(R.id.lv_filter_items);

        ArrayList<CarManufacturer> carManufacturers = CarStorage.getInstance(getContext()).getCarManufacturers();
        ArrayList<Brand> brands = CarStorage.getInstance(getContext()).getBrands();

        mFilterItemsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        final ArrayAdapter<CarManufacturer> manufacturerArrayAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_multiple_choice,
                carManufacturers);
        final ArrayAdapter<Brand> brandArrayAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_multiple_choice,
                brands);
        mFilterItemsListView.setAdapter(manufacturerArrayAdapter);

        ArrayAdapter<String> sortSpinnerAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.sort_values));
        int sortByPosition = 0;
        if (FilterPreferences.getSortByColumn(getContext()) != null) {
            switch (FilterPreferences.getSortByColumn(getContext())) {
                case CarDbSchema.CarTable.Cols.NAME:
                    sortByPosition = 1;
                    break;
                case CarDbSchema.CarTable.Cols.PRICE:
                    sortByPosition = 2;
                    break;
                case CarDbSchema.CarTable.Cols.MILEAGE:
                    sortByPosition = 3;
                    break;
            }
        }
        mSortSpinner.setAdapter(sortSpinnerAdapter);
        mSortSpinner.setSelection(sortByPosition);

        ArrayAdapter<String> filterSpinnerAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.filter_values));
        mFilterSpinner.setAdapter(filterSpinnerAdapter);
        switch (FilterPreferences.getFilterType(getContext())) {
            case FilterPreferences.FILTER_TYPE_MANUFACTURER:
                mFilterSpinner.setSelection(0);
                break;
            case FilterPreferences.FILTER_TYPE_BRAND:
                mFilterSpinner.setSelection(1);
                break;
        }
        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mFilterItemsListView.setAdapter(manufacturerArrayAdapter);
                    if (FilterPreferences.getFilterType(getContext()) == FilterPreferences.FILTER_TYPE_MANUFACTURER) {
                        ArrayList<CarManufacturer> selectedManufacturers =
                                FilterPreferences.getManufacturerFilterValues(getContext());
                        if (selectedManufacturers != null) {
                            for (CarManufacturer m : selectedManufacturers) {
                                mFilterItemsListView.setItemChecked(manufacturerArrayAdapter.getPosition(m), true);
                            }
                        }
                    }
                } else {
                    mFilterItemsListView.setAdapter(brandArrayAdapter);
                    if (FilterPreferences.getFilterType(getContext()) == FilterPreferences.FILTER_TYPE_BRAND) {
                        ArrayList<Brand> selectedBrands =
                                FilterPreferences.getBrandFilterValues(getContext());
                        if (selectedBrands != null) {
                            for (Brand b : selectedBrands) {
                                mFilterItemsListView.setItemChecked(brandArrayAdapter.getPosition(b), true);
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        b.setView(rootView);
        return b.create();
    }


    public void setOnFilterApplyListener(OnFilterApplyListener onFilterApplyListener) {
        this.mOnFilterApplyListener = onFilterApplyListener;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                applyFilter();
                break;
        }
    }

    public void applyFilter() {
        if (mOnFilterApplyListener != null) {
            String orderBy = null;
            switch (mSortSpinner.getSelectedItemPosition()) {
                case 1:
                    orderBy = CarDbSchema.CarTable.Cols.NAME;
                    break;
                case 2:
                    orderBy = CarDbSchema.CarTable.Cols.PRICE;
                    break;
                case 3:
                    orderBy = CarDbSchema.CarTable.Cols.MILEAGE;
                    break;
            }
            FilterPreferences.setSortByColumn(getContext(), orderBy);

            if (mFilterSpinner.getSelectedItemPosition() == 0) {
                ArrayList<CarManufacturer> selectedManufacturers = new ArrayList<>();
                SparseBooleanArray checkedItem = mFilterItemsListView.getCheckedItemPositions();
                for (int i = 0; i < mFilterItemsListView.getCount(); i++) {
                    if (checkedItem.get(i)) {
                        selectedManufacturers.add((CarManufacturer) mFilterItemsListView.getAdapter().getItem(i));
                    }
                }
                mOnFilterApplyListener.onApplyFilterByManufacturer(orderBy, selectedManufacturers);

                FilterPreferences.setManufacturerFilterValues(getContext(), selectedManufacturers);
            } else {
                ArrayList<Brand> selectedBrands = new ArrayList<>();
                SparseBooleanArray checkedItem = mFilterItemsListView.getCheckedItemPositions();
                for (int i = 0; i < mFilterItemsListView.getCount(); i++) {
                    if (checkedItem.get(i)) {
                        selectedBrands.add((Brand) mFilterItemsListView.getAdapter().getItem(i));
                    }
                }
                mOnFilterApplyListener.onApplyFilterByBrand(orderBy, selectedBrands);

                FilterPreferences.setBrandFilterValues(getContext(), selectedBrands);
            }
        }
    }

    interface OnFilterApplyListener {
        void onApplyFilterByBrand(@Nullable String orderBy, @Nullable ArrayList<Brand> brands);

        void onApplyFilterByManufacturer(@Nullable String orderBy, @Nullable ArrayList<CarManufacturer> manufacturers);
    }
}
