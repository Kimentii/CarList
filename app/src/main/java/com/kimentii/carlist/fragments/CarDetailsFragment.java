package com.kimentii.carlist.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.kimentii.carlist.BuildConfig;
import com.kimentii.carlist.CarImageSliderAdapter;
import com.kimentii.carlist.CarStorage;
import com.kimentii.carlist.R;
import com.kimentii.carlist.activities.CarDetailsActivity;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.Car;
import com.kimentii.carlist.dto.CarManufacturer;

import java.util.ArrayList;

import ss.com.bannerslider.Slider;

public class CarDetailsFragment extends Fragment {
    private static final String TAG = CarDetailsActivity.class.getSimpleName();
    private static final String ARGUMENT_CAR_ID = "ARGUMENT_CAR_ID";

    private Car mCar;
    private LinearLayout mRootLinearLayout;
    private boolean isEditMode = false;

    private TextView mNameTextView;
    private TextView mPriceTextView;
    private TextView mManufacturerTextView;
    private TextView mBrandTextView;
    private TextView mColorTextView;
    private TextView mMileageTextView;
    private TextView mEngineTextView;
    private TextView mTransmissionTextView;

    private EditText mNameEditText;
    private EditText mPriceEditText;
    private Spinner mManufacturerSpinner;
    private Spinner mBrandSpinner;
    private EditText mColorEditText;
    private EditText mMileageEditText;
    private EditText mEngineEditText;
    private EditText mTransmissionEditText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_details, container, false);
        mCar = CarStorage.getInstance(getContext()).getCar(getArguments().getInt(ARGUMENT_CAR_ID));
        Slider slider = view.findViewById(R.id.slider_car_images);
        slider.setAdapter(new CarImageSliderAdapter(mCar.getPhotoArrayList()));

        mRootLinearLayout = view.findViewById(R.id.ll_root);

        setUpView(view);

        updateCarDescription();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!isEditMode) {
            inflater.inflate(R.menu.action_bar_car_details, menu);
        } else {
            inflater.inflate(R.menu.action_bar_car_details_edit_mode, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ViewSwitcher vs = mRootLinearLayout.findViewById(R.id.vs_description);
        vs.showNext();
        switch (item.getItemId()) {
            case R.id.item_edit:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "onOptionsItemSelected: edit");
                }
                startEditMode();

                isEditMode = true;
                getActivity().invalidateOptionsMenu();
                break;
            case R.id.item_apply:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "onOptionsItemSelected: apply");
                }

                exitEditModeAndSaveData();
                updateCarDescription();

                isEditMode = false;
                getActivity().invalidateOptionsMenu();
                break;
        }
        return true;
    }

    public static CarDetailsFragment newInstance(int carId) {
        Bundle args = new Bundle();
        args.putInt(ARGUMENT_CAR_ID, carId);
        CarDetailsFragment fragment = new CarDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void hideKeyboard() {
        View view = getActivity().getWindow().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private void updateCarDescription() {
        mNameTextView.setText(mCar.getName());
        mPriceTextView.setText(String.valueOf(mCar.getPrice()));
        mManufacturerTextView.setText(mCar.getManufacturer().getName());
        mBrandTextView.setText(mCar.getBrand().getName());
        mColorTextView.setText(mCar.getColor());
        mMileageTextView.setText(String.valueOf(mCar.getMileage()));
        mEngineTextView.setText(String.valueOf(mCar.getEngine()));
        mTransmissionTextView.setText(mCar.getTransmission());
    }

    private void startEditMode() {
        mNameEditText.setText(mCar.getName());
        mPriceEditText.setText(String.valueOf(mCar.getPrice()));
        mColorEditText.setText(mCar.getColor());
        mMileageEditText.setText(String.valueOf(mCar.getMileage()));
        mEngineEditText.setText(mCar.getEngine());
        mTransmissionEditText.setText(mCar.getTransmission());

        CarManufacturerSpinnerAdapter cmAdapter = new CarManufacturerSpinnerAdapter(getContext());
        ArrayList<CarManufacturer> manufacturers = CarStorage.getInstance(getContext()).getCarManufacturers();
        cmAdapter.addAll(manufacturers);
        cmAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mManufacturerSpinner.setAdapter(cmAdapter);
        mManufacturerSpinner.setSelection(cmAdapter.getPosition(mCar.getManufacturer()));

        final BrandSpinnerAdapter bAdapter = new BrandSpinnerAdapter(getContext());
        ArrayList<Brand> manBrands = CarStorage.getInstance(getContext())
                .getManufacturerBrands(mCar.getManufacturer());
        bAdapter.addAll(manBrands);
        mBrandSpinner.setAdapter(bAdapter);
        mBrandSpinner.setSelection(bAdapter.getPosition(mCar.getBrand()));

        mManufacturerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CarManufacturer carManufacturer = (CarManufacturer) parent.getItemAtPosition(position);
                bAdapter.clear();
                bAdapter.addAll(CarStorage.getInstance(getContext())
                        .getManufacturerBrands(carManufacturer));
                bAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void exitEditModeAndSaveData() {
        hideKeyboard();

        mCar.setName(mNameEditText.getText().toString());
        mCar.setPrice(Integer.valueOf(mPriceEditText.getText().toString()));
        mCar.setManufacturer((CarManufacturer) mManufacturerSpinner.getSelectedItem());
        mCar.setBrand((Brand) mBrandSpinner.getSelectedItem());
        mCar.setColor(mColorEditText.getText().toString());
        mCar.setMileage(Integer.valueOf(mMileageEditText.getText().toString()));
        mCar.setEngine(mEngineEditText.getText().toString());
        mCar.setTransmission(mTransmissionEditText.getText().toString());

        CarStorage.getInstance(getContext()).updateCar(mCar);
    }

    private void setUpView(View rootView) {
        mNameTextView = rootView.findViewById(R.id.tv_name);
        mPriceTextView = rootView.findViewById(R.id.tv_price);
        mManufacturerTextView = rootView.findViewById(R.id.tv_manufacturer);
        mBrandTextView = rootView.findViewById(R.id.tv_brand);
        mColorTextView = rootView.findViewById(R.id.tv_color);
        mMileageTextView = rootView.findViewById(R.id.tv_mileage);
        mEngineTextView = rootView.findViewById(R.id.tv_engine);
        mTransmissionTextView = rootView.findViewById(R.id.tv_transmission);

        mNameEditText = rootView.findViewById(R.id.ed_name);
        mPriceEditText = rootView.findViewById(R.id.ed_price);
        mManufacturerSpinner = rootView.findViewById(R.id.spinner_manufacturer);
        mBrandSpinner = rootView.findViewById(R.id.spinner_brand);
        mColorEditText = rootView.findViewById(R.id.ed_color);
        mMileageEditText = rootView.findViewById(R.id.ed_mileage);
        mEngineEditText = rootView.findViewById(R.id.ed_engine);
        mTransmissionEditText = rootView.findViewById(R.id.ed_transmission);
    }

    private class CarManufacturerSpinnerAdapter extends ArrayAdapter<CarManufacturer> {

        CarManufacturerSpinnerAdapter(@NonNull Context context) {
            super(context, android.R.layout.simple_list_item_1);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            CarManufacturer carManufacturer = getItem(position);
            if (convertView == null) {
                convertView = getLayoutInflater()
                        .inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            if (carManufacturer != null) {
                ((TextView) convertView.findViewById(android.R.id.text1)).setText(carManufacturer.getName());
            }
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }

    private class BrandSpinnerAdapter extends ArrayAdapter<Brand> {

        BrandSpinnerAdapter(@NonNull Context context) {
            super(context, android.R.layout.simple_list_item_1);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Brand brand = getItem(position);
            if (convertView == null) {
                convertView = getLayoutInflater()
                        .inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            if (brand != null) {
                ((TextView) convertView.findViewById(android.R.id.text1)).setText(brand.getName());
            }
            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }
}
