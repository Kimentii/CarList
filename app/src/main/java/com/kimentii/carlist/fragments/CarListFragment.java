package com.kimentii.carlist.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kimentii.carlist.CarStorage;
import com.kimentii.carlist.FilterPreferences;
import com.kimentii.carlist.R;
import com.kimentii.carlist.activities.CarDetailsActivity;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.Car;
import com.kimentii.carlist.dto.CarManufacturer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CarListFragment extends ListFragment implements FilterDialogFragment.OnFilterApplyListener {
    public static final String TAG = CarListFragment.class.getSimpleName();

    private CarAdapter mCarAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateListData(getContext());
                    }
                });
            }
        });
        thread.start();
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_edit:
                FilterDialogFragment dialogFragment = new FilterDialogFragment();
                dialogFragment.setOnFilterApplyListener(this);
                dialogFragment.show(getFragmentManager(), "Filter dialog");
                break;
        }
        return true;
    }

    @Override
    public void onApplyFilterByBrand(@Nullable String orderBy, @Nullable ArrayList<Brand> brands) {
        mCarAdapter.clear();
        mCarAdapter.addAll(CarStorage.getInstance(getContext())
                .getCarsFilteredByBrand(orderBy, brands));
        mCarAdapter.notifyDataSetChanged();
    }

    @Override
    public void onApplyFilterByManufacturer(@Nullable String orderBy,
                                            @Nullable ArrayList<CarManufacturer> manufacturers) {
        mCarAdapter.clear();
        mCarAdapter.addAll(CarStorage.getInstance(getContext())
                .getCarsFilteredByManufacturer(orderBy, manufacturers));
        mCarAdapter.notifyDataSetChanged();
    }

    private void updateListData(Context context) {
        ArrayList<Car> cars = null;
        switch (FilterPreferences.getFilterType(context)) {
            case FilterPreferences.FILTER_TYPE_NONE:
                cars = CarStorage.getInstance(context).getCarsFilteredByBrand(
                        FilterPreferences.getSortByColumn(context),
                        null);
                break;
            case FilterPreferences.FILTER_TYPE_BRAND:
                cars = CarStorage.getInstance(context).getCarsFilteredByBrand(
                        FilterPreferences.getSortByColumn(context),
                        FilterPreferences.getBrandFilterValues(context));
                break;
            case FilterPreferences.FILTER_TYPE_MANUFACTURER:
                cars = CarStorage.getInstance(context).getCarsFilteredByManufacturer(
                        FilterPreferences.getSortByColumn(context),
                        FilterPreferences.getManufacturerFilterValues(context));
        }
        mCarAdapter = new CarAdapter(context, cars);
        setListAdapter(mCarAdapter);
    }

    class CarAdapter extends ArrayAdapter<Car> {

        CarAdapter(@NonNull Context context, ArrayList<Car> data) {
            super(context, R.layout.list_item_car, data);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Car car = getItem(position);
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                convertView = layoutInflater.inflate(R.layout.list_item_car, parent, false);
            }
            ImageView carImage = convertView.findViewById(R.id.iv_car_image);
            TextView carNameTextView = convertView.findViewById(R.id.tv_name);
            TextView carPriceTextView = convertView.findViewById(R.id.tv_price);

            convertView.setOnClickListener(new CarItemClickListener(car));
            if (car.getPhotoArrayList() != null && !car.getPhotoArrayList().isEmpty()) {
                Picasso.get().load(car.getPhotoArrayList().get(0).getPhotoUrl())
                        .error(R.drawable.ic_launcher_background).into(carImage);
            } else {
                carImage.setImageResource(R.drawable.no_image);
            }
            carNameTextView.setText(car.getName());
            carPriceTextView.setText(String.valueOf(car.getPrice()));
            return convertView;
        }

        private class CarItemClickListener implements View.OnClickListener {
            private Car mCar;

            CarItemClickListener(Car car) {
                mCar = car;
            }

            @Override
            public void onClick(View view) {
                Intent intent =
                        CarDetailsActivity.getStartIntent(getContext(),
                                mCar.getCarId());
                startActivity(intent);
            }
        }
    }
}
