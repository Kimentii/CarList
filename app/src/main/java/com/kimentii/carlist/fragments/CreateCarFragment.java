package com.kimentii.carlist.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kimentii.carlist.BuildConfig;
import com.kimentii.carlist.CarStorage;
import com.kimentii.carlist.R;
import com.kimentii.carlist.activities.CarDetailsActivity;
import com.kimentii.carlist.dto.Brand;
import com.kimentii.carlist.dto.Car;
import com.kimentii.carlist.dto.CarManufacturer;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import ss.com.bannerslider.Slider;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class CreateCarFragment extends Fragment {
    private static final String TAG = CarDetailsActivity.class.getSimpleName();

    private static final int REQUEST_PHOTO = 1;

    private CarStorage mCarStorage;

    private Slider mCarImagesSlider;

    private EditText mNameEditText;
    private EditText mPriceEditText;
    private Spinner mManufacturerSpinner;
    private Spinner mBrandSpinner;
    private EditText mColorEditText;
    private EditText mMileageEditText;
    private EditText mEngineEditText;
    private EditText mTransmissionEditText;
    private ArrayList<EditText> mAllEditTextFields;
    private Drawable mNormalEditTextBackground;

    private File mPhotoFile;
    private ArrayList<String> mCarImages = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_car, container, false);

        mCarStorage = CarStorage.getInstance(getContext());

        AppCompatButton addPhotoButton = view.findViewById(R.id.button_add_photo);
        setUpView(view);

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhotoFile = mCarStorage.getPhotoFile(getContext());
                final Intent captureImageIntent = new Intent
                        (MediaStore.ACTION_IMAGE_CAPTURE);
                boolean canTakePhoto = mPhotoFile != null &&
                        captureImageIntent.resolveActivity(getActivity().getPackageManager()) != null;
                v.setEnabled(canTakePhoto);
                if (canTakePhoto) {
                    Uri apkURI = FileProvider.getUriForFile(
                            getContext(),
                            getContext().getApplicationContext()
                                    .getPackageName() + ".provider", mPhotoFile);
                    captureImageIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    captureImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);
                }
                startActivityForResult(captureImageIntent, REQUEST_PHOTO);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_car_details_edit_mode, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_apply:
                if (checkInputedData()) {
                    createCarAndSaveInDatabase();
                    getActivity().finish();
                } else {
                    Toast.makeText(getContext(), R.string.warning_empty_fields, Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_PHOTO) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onActivityResult: " + mPhotoFile.getAbsolutePath());
            }
            mCarImages.add("file://" + mPhotoFile.getAbsolutePath());
            // notifyDataSetChanged analog
            mCarImagesSlider.setAdapter(new CarImageSliderAdapter());
        }
    }

    public static CreateCarFragment newInstance() {
        return new CreateCarFragment();
    }

    private void setUpView(View rootView) {
        // text fields
        mNameEditText = rootView.findViewById(R.id.ed_name);
        mPriceEditText = rootView.findViewById(R.id.ed_price);
        mColorEditText = rootView.findViewById(R.id.ed_color);
        mMileageEditText = rootView.findViewById(R.id.ed_mileage);
        mEngineEditText = rootView.findViewById(R.id.ed_engine);
        mTransmissionEditText = rootView.findViewById(R.id.ed_transmission);
        mNormalEditTextBackground = mNameEditText.getBackground();

        // sliders
        mCarImagesSlider = rootView.findViewById(R.id.slider_car_images);
        // spinners
        mManufacturerSpinner = rootView.findViewById(R.id.spinner_manufacturer);
        mBrandSpinner = rootView.findViewById(R.id.spinner_brand);

        mAllEditTextFields = new ArrayList<>();
        mAllEditTextFields.add(mNameEditText);
        mAllEditTextFields.add(mPriceEditText);
        mAllEditTextFields.add(mColorEditText);
        mAllEditTextFields.add(mMileageEditText);
        mAllEditTextFields.add(mEngineEditText);
        mAllEditTextFields.add(mTransmissionEditText);

        mCarImagesSlider.setAdapter(new SliderAdapter() {
            @Override
            public int getItemCount() {
                return 1;
            }

            @Override
            public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {
                imageSlideViewHolder.imageView.setImageResource(R.drawable.no_image);
            }
        });

        ArrayList<CarManufacturer> manufacturers = mCarStorage.getCarManufacturers();
        ArrayAdapter<CarManufacturer> mAdapter = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                manufacturers);
        mManufacturerSpinner.setAdapter(mAdapter);
        mManufacturerSpinner.setSelection(mAdapter.getPosition(manufacturers.get(0)));

        final ArrayAdapter<Brand> bAdapter = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_1,
                mCarStorage.getManufacturerBrands(manufacturers.get(0)));
        mBrandSpinner.setAdapter(bAdapter);
        mManufacturerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CarManufacturer carManufacturer = (CarManufacturer) parent.getItemAtPosition(position);
                bAdapter.clear();
                bAdapter.addAll(mCarStorage.getManufacturerBrands(carManufacturer));
                bAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private boolean checkInputedData() {
        boolean allFieldsFilled = true;
        for (EditText editText : mAllEditTextFields) {
            if (editText.getText().toString().trim().isEmpty()) {
                editText.setBackgroundResource(R.drawable.edit_text_red_bg);
                allFieldsFilled = false;
            } else {
                editText.setBackground(mNormalEditTextBackground);
            }
        }
        return allFieldsFilled;
    }

    private void createCarAndSaveInDatabase() {
        Car car = new Car();
        car.setName(mNameEditText.getText().toString());
        car.setPrice(Integer.valueOf(mPriceEditText.getText().toString()));
        car.setColor(mColorEditText.getText().toString());
        car.setMileage(Integer.valueOf(mMileageEditText.getText().toString()));
        car.setEngine(mEngineEditText.getText().toString());
        car.setTransmission(mTransmissionEditText.getText().toString());

        car.setBrand((Brand) mBrandSpinner.getSelectedItem());
        car.setManufacturer((CarManufacturer) mManufacturerSpinner.getSelectedItem());

        long carId = mCarStorage.addCar(car);
        car.setCarId((int) carId);
        for (String url : mCarImages) {
            mCarStorage.addCarPhoto(carId, url);
        }
    }

    private class CarImageSliderAdapter extends SliderAdapter {

        @Override
        public int getItemCount() {
            return mCarImages.size();
        }

        @Override
        public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {
            Picasso.get().load(mCarImages.get(position))
                    .error(R.drawable.no_image).into(imageSlideViewHolder.imageView);
        }
    }

}
