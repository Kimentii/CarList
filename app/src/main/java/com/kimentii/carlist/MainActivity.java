package com.kimentii.carlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kimentii.carlist.activities.CreateCarActivity;
import com.kimentii.carlist.fragments.CarListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Fragment listFragment = new CarListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_car_list, listFragment).commit();

        FloatingActionButton addCarButton = findViewById(R.id.fab_add_car);
        addCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = CreateCarActivity.getStartIntent(MainActivity.this.getApplicationContext());
                startActivity(i);
            }
        });
    }
}
